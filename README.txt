The Revision Reference URL Widget module adds a new widget to the Revision Reference field type. It auto-populates a node reference field with a value from the URL, and does not allow this value to be changed once set.

Dependencies
------------
 * Revision Reference: http://drupal.org/project/revisionreference

Install
-------
1) Copy the revisionreference_url folder to the sites/all/modules folder in your
   installation.

2) Enable the module using Administer -> Modules (admin/modules)

3) Add or edit a Revision Reference field from admin/structure/types/manage/[type]/fields.
   When configuring the field, use the "Reference from URL" option.

4) Follow on-screen help text to configure your Revision Reference URL Widget.

@TODOs
------
